"use strict";
var icons = [
  "http://news.lovelive-anime.jp/app-def/S-102/news/wp-content/uploads/2012/09/honoka_icon5000.jpg",
  "http://news.lovelive-anime.jp/app-def/S-102/news/wp-content/uploads/2012/09/eli_icon5000.jpg",
  "http://news.lovelive-anime.jp/app-def/S-102/news/wp-content/uploads/2012/09/kotori_icon5000.jpg",
  "http://news.lovelive-anime.jp/app-def/S-102/news/wp-content/uploads/2012/09/umi_icon5000.jpg",
  "http://news.lovelive-anime.jp/app-def/S-102/news/wp-content/uploads/2012/09/rin_icon5000.jpg",
  "http://news.lovelive-anime.jp/app-def/S-102/news/wp-content/uploads/2012/09/maki_icon5000.jpg",
  "http://news.lovelive-anime.jp/app-def/S-102/news/wp-content/uploads/2012/09/nozomi_icon5000.jpg",
  "http://news.lovelive-anime.jp/app-def/S-102/news/wp-content/uploads/2012/09/hanayo_icon5000.jpg",
  "http://news.lovelive-anime.jp/app-def/S-102/news/wp-content/uploads/2012/09/nico_icon5000.jpg"
];
var f = function() {
  var xhr = new XMLHttpRequest();
  xhr.responseType = "document";
  xhr.onload = function() {
    var rp = this.response.getElementsByClassName("post");
    var posts = JSON.parse(localStorage.getItem("posts")) || [];
    var ids = {};
    for (var i = 0; i < rp.length; i++) {
      var id = parseInt(rp[i].id.split("-")[1]);
      if (!i || posts.indexOf(id) < 0) {
        var icon = icons[Math.floor(Math.random() * 9)];
        var title = rp[i].getElementsByClassName("date")[0].textContent;
        var body = rp[i].getElementsByTagName("h2")[0].textContent;
        chrome.notifications.create("", {
          type: "basic",
          title: title,
          message: body,
          iconUrl: icon
        }, (function (id) {
          return function (notificationId) {
            ids[notificationId] = id;
          };
        }(id)));
      }
    }
    var clicked = function (notificationId) {
      var id = ids[notificationId];
      chrome.notifications.clear(notificationId, function () {});
      chrome.tabs.create({"url": "http://news.lovelive-anime.jp/app-def/S-102/news/?p=" + id});
      if (posts.indexOf(id) < 0) {
        posts.push(id);
      }
      localStorage.setItem("posts", JSON.stringify(posts));
    };
    chrome.notifications.onClicked.addListener(clicked);
    setTimeout((function (clicked) {
      return function () {
        chrome.notifications.getAll(function (notifications) {
          for (var notificationId in notifications) {
            chrome.notifications.clear(notificationId, function () {});
          }
        });
        chrome.notifications.onClicked.removeListener(clicked);
      };
    }(clicked)), 300000);
  };
  xhr.open("GET", "http://news.lovelive-anime.jp/app-def/S-102/news/");
  xhr.send();
};
f();
setInterval(f, 3600000);
